import React from "react";
import '/node_modules/@ndmspc/react-ndmspc-patternfly/dist/style.css';
import { NdmspcApp } from "@ndmspc/react-ndmspc-patternfly";
import { RsnBinPlugin, RsnProjectionPlugin } from "@ndmspc/react-ndmspc-plugins";

export default function App() {
  return (<NdmspcApp plugins={{ RsnBinPlugin, RsnProjectionPlugin }} />);
}