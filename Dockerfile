FROM node:18 AS builder
WORKDIR /builder
COPY package*.json /builder/
COPY vite*.js /builder/
COPY index.html /builder/
COPY src /builder/src
RUN npm i
RUN npm run build

FROM docker.io/nginxinc/nginx-unprivileged:alpine
WORKDIR /usr/share/nginx/html
COPY --from=builder /builder/dist/ /usr/share/nginx/html
RUN ls -al /usr/share/nginx/html
